### Import libraries
### Import files
import geometry as geom
import read_parameters_ver2 as readpara
import Energy_v2 as energy

############# BEGIN MAIN CODE


### Define filename
fname = "../test/ethene"  # for example


############# INITIAL STUFF
### Import geometry; read in coordinates and connectivities of atoms
coordinates, connect = geom.mol_info(fname)
atoms, nlines = geom.input_geom(fname)
interactions_list = geom.translate(connect, atoms)
### Construct nearest neighbour list
#NNlist = func(atom_list)
### Using this list, figure out which interactions are being used
### Read file of interactions, put into stored_interactions
""" stored interactions is a dictionary where the keys include
Hcoeff and Scoeff and the corresponding value is an array of the
corresponding interactions; also included are the key spline and
the corresponding value gives the parameters for spline
"""

stored_interactions = {}
for interaction in interactions_list:
    stored_interactions[interaction] = readpara.readfile(interaction)


############# GETTING H AND S
# Using interaction list, find H
#energy(stored_interactions)
# Do rotations

############## Energy Calculations
# Calculate energy
# Plot spectrum
#### Could maybe do an SCF loop etc?
