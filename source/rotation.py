"""Module to rotate the elements of the overlap and Hamiltonian matrices"""

import numpy as np
import sys

def rotation_coefficients(p_orbitals_1, p_orbitals_2, r_hat):
    """Calculates the coefficients c_tau in equation 135."""

    # If only s orbitals, the function returns 1

    # If there is one s orbital and one p orbital, function returns:
    # c[0] = spx
    # c[1] = spy
    # c[2] = spz

    # If both are p, returns:
    # c[0, 0] = pxpx sigma
    # c[0, 1] = pxpy sigma
    # c[0, 2] = pxpz sigma
    # c[0, 3] = pypy sigma
    # c[0, 4] = pypz sigma
    # c[0, 5] = pzpz sigma
    # c[1, :] = as above but pi

    if np.dot(r_hat, r_hat) != 1.0:
        sys.exit("r_hat rotation_coefficients needs to be a unit vector ")

    if not p_orbitals_1 and not p_orbitals_2:
        return None

    elif (p_orbitals_1 and not p_orbitals_2) or (not p_orbitals_1 and p_orbitals_2):
        c = np.zeros(3)
        c[0] = r_hat[0]
        c[1] = r_hat[1]
        c[2] = r_hat[2]
        return c

    else:
        c = np.zeros([2, 6])
        c[0, 0] = r_hat[0] ** 2.0
        c[0, 1] = r_hat[0] * r_hat[1]
        c[0, 2] = r_hat[0] * r_hat[2]
        c[0, 3] = r_hat[1] ** 2.0
        c[0, 4] = r_hat[1] * r_hat[2]
        c[0, 5] = r_hat[2] ** 2.0

        c[1, :] = -1.0 * c[0, :]
        c[1, 0] += 1.0
        c[1, 3] += 1.0
        c[1, 5] += 1.0

        return c



def rotate_bond(p_orbitals_1, p_orbitals_2, r_hat, elements):
    """Rotates an interaction between two orbitals"""

    if not p_orbitals_1 and not p_orbitals_2:
        return elements

    elif (p_orbitals_1 and not p_orbitals_2) or (not p_orbitals_1 and p_orbitals_2):
        c = rotation_coefficients(p_orbitals_1, p_orbitals_2, r_hat)

        new_elements = c * elements
        return new_elements

    else:
        c = rotation_coefficients(p_orbitals_1, p_orbitals_2, r_hat)

        new_elements = np.zeros([3, 3])

        new_elements[0, 0] = c[0, 0] * elements[0] + c[1, 0] * elements[1]
        new_elements[0, 1] = c[0, 1] * elements[0] + c[1, 1] * elements[1]
        new_elements[0, 2] = c[0, 2] * elements[0] + c[1, 2] * elements[1]
        new_elements[1, 1] = c[0, 3] * elements[0] + c[1, 3] * elements[1]
        new_elements[1, 2] = c[0, 4] * elements[0] + c[1, 4] * elements[1]
        new_elements[2, 2] = c[0, 5] * elements[0] + c[1, 5] * elements[1]
        return new_elements