import pyglet
from pyglet.gl import *
from math import *
from random import randint
import random
import numpy

global FPS
FPS = 1

global Energies
Energies = [-random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random()]

class drawWindow(pyglet.window.Window):
    def __init__(self):
        super(drawWindow, self).__init__()
        self.Energies = [-random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random()]

    def draw_lines(self):
        maxenergy = min(self.Energies)
        maxheight =  - window.height * 2.0/3.0
        for energy in self.Energies:
            height = (energy / maxenergy) * maxheight
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                             ('v2i', (self.linewidth, int(height - maxheight + 50), 2 * self.linewidth, int(height - maxheight + 50))))
            pyglet.text.Label(str(numpy.round(energy,3)),
                              font_name='Comic Sans MS',
                              font_size=8,
                              x=self.linewidth - 50, y=int(height - maxheight + 50),
                              anchor_x='center', anchor_y='center').draw()

    def draw_molecule(self, fname, molecule_width):
        coords, connect = mol_info("../test/" + fname)
        max_dist = np.amax(pdist(coords))
        min_val = np.amin(coords)
        coords = (((coords - min_val) / max_dist) * molecule_width) + 50
        print coords
        drawlist = []
        i = 0
        while i < len(coords):
            drawlist.append(makeCircle(100, 5, list(coords[i])[0], list(coords[i])[1]))
            i += 1
        for element in drawlist:
            glColor3f(1, 1, 0)
            element.draw(GL_LINE_LOOP)
        i = 0
        while i < len(connect):
            bonds = connect[i + 1]
            for bond in bonds:
                x1 = int(list(coords[i])[0])
                x2 = int(list(coords[bond - 1])[0])
                y1 = int(list(coords[i])[1])
                y2 = int(list(coords[bond - 1])[1])
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                                 ('v2i', (x1, y1, x2,
                                          y2)))
            i += 1

    def on_draw(self):
        self.clear()
        self.label = pyglet.text.Label('Energy Spectrum',
                              font_name='Comic Sans MS',
                              font_size=36,
                              x=window.width / 2, y=window.height * 5.0 / 6.0,
                              anchor_x='center', anchor_y='center')
        self.label.draw()
        self.linewidth = window.width / 3
        self.draw_lines()

    def update(self,dt):
        #print(dt) # time elapsed since last time a draw was called
        #self.center1 = [window.width/2 + randint(-200,200), window.height/2 + randint(-200,200)]
        #self.center2 = [window.width/2 + randint(-200,200), window.height/2 + randint(-200,200)]
        self.Energies = [-random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random(), -random.random()]

if __name__ == '__main__':
    window = drawWindow()                                 # initialize a window class
    pyglet.clock.schedule_interval(window.update, 1/FPS)  # tell pyglet how often it should execute on_draw() & update()
    pyglet.app.run()# run pyglet