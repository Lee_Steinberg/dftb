import numpy as np


def readfile(filename):
    '''function to read data from a single parameter file
    ouputs into a dictionary structure, which will have
    an entry for each interaction pair. This function only wroks for homonuclear pairs '''
    dict = {}
    fileloc = "../mio-1-1/" + filename + ".skf"
    lines = open(fileloc, "r").readlines()
    line1 = lines[0].split(',')
    line2 = lines[1].split()
    for index, item in enumerate(line2):
        line2[index]=item.replace(",", "")

    grid_space = float(line1[0])
    grid_no = int(line1[1])

    mass = float(lines[2].split()[0].replace(",",""))
    dict["mass"] = mass
    dict["E"] = np.array(line2[:3], dtype = float)
    dict["occupancies"] = np.array(line2[-3:], dtype=float)
    matrix_start = 22
    Hcoeff=[]
    Scoeff=[]

    ##read in parameters for H(r) and S(r), where r is distance##
    column_num = len(lines[matrix_start].split())
    parse1 = 0
    parse2 = int(column_num / 2.)
    for line in lines[matrix_start:matrix_start+grid_no]:
        words = line.split()
        Hcoeff.append(words[parse1+1:parse2])
        Scoeff.append(words[parse2+1:])


    Scoeff = make_matrix(Scoeff, grid_space, grid_no )
    Hcoeff = make_matrix(Hcoeff, grid_space, grid_no )

    ##load in spline parameters##

    dict["spline"] = {}

    dict["spline"] = read_spline(dict["spline"], lines)


    ##store things in dictionary##
    dict["Hcoeff"] = Hcoeff
    dict["Scoeff"] = Scoeff
    return dict


def read_spline(dict, lines ):
    '''Returns a dictionary with all spline information contained within.
    "cutoff" key cotains the replusive cutoff. Note that
    all spline functions are in the form [lower_limit, upper_limit, *coeffients...]
    Each type of spline function is stored in a seperate key.
    "exp" contains the exponential terms.
    "quad" contains the quartic spline parameters for all distance intervals
    "sext" contains the sixth order polynomial spline function '''
    spline_start=0
    for num, line in enumerate(lines):
        if "Spline" in line:
            spline_start = num
            exit
    line2 = lines[spline_start + 1].split()
    n_int = int(line2[0]) #number for intpol sections
    cutoff = float(line2[1])  # cutoff of the repulsive interaction
    spline_dat = lines[spline_start+1:spline_start + 3+n_int]

    for ind, item in enumerate(spline_dat):
        spline_dat[ind] = item.split()


    splines = np.array(spline_dat[2:2+n_int - 1], dtype=float)




    exp_spline = np.array(spline_dat[1], dtype=float)

    spline_last = np.array(spline_dat[-1], dtype=float)


    exp_spline = np.append(np.array([0., splines[0,0]]), exp_spline )

    dict["cutoff"] = cutoff
    dict["exp"] = exp_spline
    dict["quad"] = splines
    dict["sext"] = spline_last

    return dict



##function to make a 2D array where a single row reads [r, H(r)/s(r)]##
def make_matrix(coeff, grid_space, grid_no):
    '''function to make a 2D array where a single row reads [r, H(r)/s(r)]
    where r is the internuclear distance'''
    r = np.arange(1, grid_no+1)*grid_space
    coeff = np.array(coeff, dtype=float)
    matrix = np.concatenate((r[:, np.newaxis], coeff), axis=1)
    return matrix



if __name__ == "__main__":
    dict = readfile("C-C")
    #print dict["spline"], '\n'
    #print dict["occupancies"], dict["E"]
