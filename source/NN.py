from geometry import *
import numpy as np

def NNeighbours(pair_dist):
    """ return nearest neighbours from a matrix of distance;
    return the shortest distance between nearest neighbours
    and the index of the atom that is nearest to it """
    min_dist = []  # list used to store shortest distance and index of the atom
    pair_dist = pair_dist + 10000*np.eye(len(pair_dist)) # add large numbers along diagonal so that
                                                         # it can never be minimum distance
    for row in pair_dist:
        a = np.min(row)
        b = np.argmin(row)
        min_dist.append([a,b])
    return min_dist

