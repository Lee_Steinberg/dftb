# -*- coding: utf-8 -*-
from numpy import *
import pylab as pl
import csv
from scipy import *
from scipy import linalg
from math import *
import read_parameters_ver2 as rp
from interpolate_tests import *

# reading data


dict1 = rp.readfile("H-H")

r=list(dict1["Hcoeff"][:, 0].flatten())
H_ij= list(dict1["Hcoeff"][:, 1:].flatten())
S_ij = list(dict1["Scoeff"][:, 1:].flatten())

'''
f2=open('/Users/wenghongsio/Desktop/DFT/dftb/Energy/Coefficients.csv','r') # used to open csv files
data2= []

lb,up,C0,C1,C2,C3=[],[],[],[],[],[]

for row in csv.reader(f2):
    data2.append(row)

for n in range(len(data2)):
    lb.append(float(data2[n][0]))
    up.append(float(data2[n][1]))
    C0.append(float(data2[n][2]))
    C1.append(float(data2[n][3]))
    C2.append(float(data2[n][4]))
    C3.append(float(data2[n][5]))


f2.close()

def E_Repulsive(R):   
    for i in range(len(lb)):
        if R <= lb[0]:
            a1,a2,a3 = 3.729040602121917, 1.528691797102741, -0.02094423834462684
            return exp(-a1*R + a2) + a3
            
        elif lb[0] < R <= up[-1]:
            if lb[i] < R <= up[i]:
                #print(R,C0[i],C1[i],C2[i],C3[i])
                return C0[i] + (C1[i] * R) + (C2[i] * R**2) + (C3[i]*R**3)
        
        else:
            print(lb[i] ,up[i])
            print ("Outside regime for repulsive %s" % R)
       
'''  
            
# Constructing Hamiltonian matrix and Overlap matrix S
def Band_Energy(R):
    index = r.index(R)      # extracting index of bond length
    H12 = H_ij[index]       # input Hamiltonian with 
    H21 = H12
    H11 = -0.23860040
    H22 = -0.23860040
    S11,S22 = 1.0, 1.0
    S12 = S_ij[index]
    S21 = S12
    # intitalising H and S matrix 
    H,S=array([[0.0]*2]*2), array([[0.0]*2]*2)
    H[0][0]=H11
    H[0][1]=H12
    H[1][0]=H21
    H[1][1]=H22

    S[0][0]=S11
    S[0][1]=S12
    S[1][0]=S21
    S[1][1]=S22
    
    # calculate eigenvalues of energy Ea by transformation 
    eigvalue, U = linalg.eigh(S)
    Ds = diag(real(eigvalue))
    Ds_half = diag((real(eigvalue))**-0.5)
    X = dot( U , dot((Ds_half) , U.transpose()))
    H_prime = dot(X.transpose(), dot( mat(H) , mat(X)) )
    eig = list(linalg.eigh(H_prime)[0])
    #print (eig)
    fa=[2,0]

    Eband_energy = 0.0
    for i in range(len(eig)):
        Eband_energy = Eband_energy + fa[i]*eig[i]
    
    #pairwise repulsive term
    #Erep = E_Repulsive(R)
    '''
    # pairwise repulsive term
    a1,a2,a3 = 3.729040602121917, 1.528691797102741, -0.02094423834462684
    E_repulsive = exp(-a1*R + a2) + a3
    E = Eband_energy + E_repulsive
    '''
    return Eband_energy
    #return [Eband_energy, Erep,Eband_energy+Erep]

bond_length,E0=[],[]



i=0
while True:
    bond_length.append(r[i])
    E0.append(Band_Energy(r[i]))
    if r[i] > 1.8:
        break
    i = i + 1

energy = []    
[r_s,spline] = make_spline(40,1.04,0.02)      
spline_new = interpolate_spline(spline,r_s)
 
E_T = []
i = 0
while i < len(E0):
    E_b = E0[i]
    print E_b
    E_s = spline_new(r_s)[i]
    print E_s
    E_T.append(E_b + E_s)
    i += 1
    
E_T = list(E_T)
pl.plot(bond_length,E_T,'r-')
pl.show()

