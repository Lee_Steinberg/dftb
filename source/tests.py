import geometry

def test_input_geom():
    assert geometry.input_geom("../test/H2") == [{'atomic number': 1.0, 'ang_mom_p': False, 'position vector': [0.0, 0.0, 0.0], 'ang_mom_s': True}, {'atomic number': 1.0, 'ang_mom_p': False, 'position vector': [1.04, 0.0, 0.0], 'ang_mom_s': True}]