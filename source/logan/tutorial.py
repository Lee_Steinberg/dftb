import pyglet
from pyglet.gl import *
from math import *
import numpy as np
from random import randint

window = pyglet.window.Window()
global drawList
dvd = pyglet.resource.image('logan.png')
dvd.width = 100
dvd.height = 100
ncircles = 5
drawList = [0]*ncircles
global time_lee, r # Initial time and circle path radius
time_lee = 0 # Initial time
global switch
switch = 1
global switcha
global switchb
switcha = 1
switchb = -1
r = window.width/3 # Circle path radius
r2 = window.width / 4
r3 = window.width / 10
delta = pi * 4/3
global imagepos
imagepos = [100,100]
global center1, center2, center3, center4, center5
center1 = [window.width/2, window.height/2]
center2 = [window.width/2, window.height/2]
center3 = [window.width/2, window.height/2]
center4 = [window.width/2, window.height/2]
center5 = [window.width/2, window.height/2]

#print window.width, window.height

def makeCircle(numPoints, radius, xcenter, ycenter):
    vertices = []
    for i in range(numPoints):
        angle = radians(float(i)/numPoints * 360.0)
        x = radius*cos(angle) + xcenter
        y = radius*sin(angle) + ycenter
        vertices += [x,y]
    circle = pyglet.graphics.vertex_list(numPoints, ('v2f', vertices))
    return circle

@window.event
def on_draw():
    #
    # drawList[0] = makeCircle(100, 20, center1[0], center1[1])
    # drawList[1] = makeCircle(100, 50, center2[0], center2[1])
    # drawList[2] = makeCircle(100,10 * (center3[0]) / 100, center3[0],center3[1])
    # drawList[3] = makeCircle(100,10, center4[0],center4[1])
    # drawList[4] = makeCircle(100,10, center5[0],center5[1])
    #
    # glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
    #
    # glColor3f(1,1,0)
    # drawList[0].draw(GL_LINE_LOOP)
    #
    # glColor3f(0.5,0,1)
    # drawList[1].draw(GL_LINE_LOOP)
    #
    # glColor3f(0.5,1,1)
    # drawList[2].draw(GL_LINE_LOOP)

    #glColor3f(0.5,0.4,0.1)
    #drawList[3].draw(GL_LINE_LOOP)

    #glColor3f(1,1,1)
    #drawList[4].draw(GL_LINE_LOOP)
    window.clear()
    dvd.blit(imagepos[0], imagepos[1])
    dvd.blit(center1[0], center1[1])
    dvd.blit(center5[0], center5[1])
    dvd.blit(center3[0], center3[1])
    dvd.blit(center2[0], center2[1])

def update(dt):
    global time_lee
    global switch
    global imagepos
    global switcha, switchb
    #print(dt) # time elapsed since last time a draw was called
    #print "Execute Some Function here to update the centers of the circles"
    center1[0] = window.width/2 + r * cos(time_lee)
    center1[1] = window.height/2 + r * sin(time_lee)
    center5[0] = center1[0] + r3 * cos(-time_lee * 5)
    center5[1] = center1[1] + r3 * sin(-time_lee * 5)
    center3[0] = (window.width + r2 * cos((-time_lee) * 3/2 + delta)) / 2
    center3[1] = window.height/2 + r2 * sin((-time_lee) * 3/2 + delta)
    imagepos[0] = imagepos[0] + 4 * switcha
    imagepos[1] = imagepos[1] + 2 * switchb
    if imagepos[0] > window.width -100 or imagepos[0] < 0:
        switcha = -switcha
    if imagepos[1] > window.height - 100 or imagepos[1] < 0 :
        switchb = -switchb
    #center4[0] = center4[0] + 5 * switch
    #if center4[0] > window.width or center4[0] < 0:
    #    switch = -switch
    time_lee += dt
    # center1[0] = window.width/2 + randint(-200,200)
    # center1[1] = window.height/2 + randint(-200,200)
    # center2[0] = window.width/2 + randint(-200,200)
    # center2[1] = window.height/2 + randint(-200,200)


if __name__ == '__main__':
    pyglet.clock.schedule(update) # cause a timed event as fast the architecture allows!
#   pyglet.clock.schedule_interval(update, 1/2.0) # update at 2Hz
    pyglet.app.run()
