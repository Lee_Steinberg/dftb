from numpy import *
from pylab import *
import csv
from scipy import *
from scipy import linalg
from scipy.integrate import quad
from math import *

# reading data
f=open('/Users/wenghongsio/Desktop/DFT/dftb/Energy/Matrix_element.csv','r') # used to open csv files

data=[]
r=[]
H_ij=[]
S_ij=[]

for row in csv.reader(f):
    data.append(row)
    
for n in range(len(data)):
    r.append(float(data[n][2])) # make string to float object
for a in range(len(data)):
    H_ij.append(float(data[a][0]))
    S_ij.append(float(data[a][1]))

f.close()

# Constructing Hamiltonian matrix and Overlap matrix S
R=1.04
index = r.index(R)

H12 = H_ij[index]
H21 = H12
H11 = -0.23860040
H22 = -0.23860040

S11,S22 = 1.0, 1.0
S12 = S_ij[index]
S21 = S12

H,S=array([[0.0]*2]*2), array([[0.0]*2]*2)
H[0][0]=H11
H[0][1]=H12
H[1][0]=H21
H[1][1]=H22

S[0][0]=S11
S[0][1]=S12
S[1][0]=S21
S[1][1]=S22

# calculate eigenvalues of energy Ea by transformation 
eigvalue, U = linalg.eigh(S)
Ds = diag(real(eigvalue))
Ds_half = diag((real(eigvalue))**-0.5)
X = dot( U , dot((Ds_half) , U.transpose()))
#print("Smh")
#print(X)
H_prime = dot(X.transpose(), dot( mat(H) , mat(X)) )
#print(H_prime)
#print dot(Ds_half,dot(Ds,Ds_half))
eig = list(linalg.eigh(H_prime)[0])
print (eig)
'''
eigvalues, U = linalg.eigh(H)
print eigvalues
'''
fa=[2,0]

Eband_energy = 0.0
for i in range(len(eig)):
    Eband_energy = Eband_energy + fa[i]*eig[i]

#print Eband_energy

# pairwise repulsive term
a1,a2,a3 = 3.729040602121917, 1.528691797102741, -0.02094423834462684

E_repulsive = exp(-a1*R + a2) + a3

E = Eband_energy + E_repulsive

print Eband_energy, E_repulsive

