import numpy as np
from numpy import linalg as LA

### MATRIX MANIPULATION CALCULATIONS

def diagonalise1(M):
    """This function diagonalises a matrix M and returns the diagonalised matrix
    and its right eigenvectors as a list [vals,vecs]
    - uses numpys eigenvalues and eigenvector routine"""
    vals, vecs = LA.eig(M)
    return [vals,vecs]   

def orthog_lowdin(S):
    """Orthogonalises a matrix based on Lowdin orthogonalisation. Takes the
    overlap matrix, S, and returns S^(-1/2). Multiply the basis by this to
    get an orthogonal basis"""
    dum = diagonalise1(S) # Diagonalise the matrix, put in dummy variable
    sqrtSdiag =  np.diag(1.0/np.sqrt(dum[0]))
    Sdiag = np.dot(np.dot(np.transpose(dum[1]),sqrtSdiag),dum[1])
    return Sdiag


    
