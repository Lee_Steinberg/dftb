import scipy.spatial.distance as scidist
import numpy as np


def input_geom(fname): # function takes filename as an argument without extension

    """ Read input file line by line (except header line)
    and store atomic number and position vectors.
    Return these as a list of dictionaries"""

    atoms = []
    fname = fname + ".xyz"
    nlines = 0

    with open(fname) as f:
        lines = f.readlines()[1:] # ignore the first line
        cnt=1
        for line in lines:
            if len(line) == 1: # if it encounters an empty line, stop
                break
            nlines += 1 # nlines gives totoal number of atoms
            words = line.split()
            atom = {"index": cnt, "atomic number" : float(words[0]), "position vector": [float(words[1]), float(words[2]), float(words[3])], "ang_mom_s": True, "ang_mom_p": False} # create a dictionary for each line
            cnt += 1
            if atom["atomic number"] == 6:
                atom["ang_mom_s"] = True
                atom["ang_mom_p"] = True
            atoms.append(atom)
    return atoms, nlines # return a list of dictionaries each containing information of each atom.
                         # return nlines which is the number of atoms

def connectivity(fname,nlines):
    """ gives connectivities of atoms in a dictionary;
    atom connnectivity redundancy is not included"""
    fname = fname + ".xyz"
    connect ={}
    with open(fname) as f:
        lines = f.readlines()[nlines+2:] # start reading lines from where connectivity starts
        for line in lines:
            words = line.split()
            #if len(words) != 1:
            connect[int(words[0])] = [int(words[i]) for i in range(1,len(words))] # stores a dictionary containing the connectivities of atoms
    return connect

def mol_info(fname):
    """ return geometry coordinates and atom connectivities;
    coordinates is in the form of a list of dictionaries where each dictionary is an atom,
    with key as the atom number and the values as the coordinates.
    atom connectivities is in the form of a dictionary"""
    coordinates = []
    atoms, nlines = input_geom(fname)      # no extension needed when file
    connect = connectivity(fname, nlines)  # is passed to these two functions
    for i in range(len(atoms)):
        dict = atoms[i]
        pair_coord = dict["position vector"]
        coordinates.append(pair_coord)
    return coordinates, connect

def pdist(coordinates): ## coordinates is a list of lists each containing the coordinates of an atom
    """ finds pairwise distance between atoms and
    use this to find the nearest neighbour """
    pair_dist = scidist.pdist(coordinates, 'euclidean') # finding pairwise distance as a condensed distance matrix
    pair_dist = scidist.squareform(pair_dist)
    return pair_dist

def translate(connect, atoms):
    dict_elements = {1.0:"H", 6.0:"C"}
    interactions = []
    for key, value in connect.iteritems():
        dict_atom1 = atoms[key-1]
        Z_num1 = dict_atom1['atomic number']
        element1 = dict_elements[Z_num1]
        for item in value:
            dict_atom2 = atoms[int(item)-1]
            Z_num2 = dict_atom2['atomic number']
            element2 = dict_elements[Z_num2]
            interact = element1 + "-" + element2
            if interact not in interactions:
                interactions.append(interact)
    return interactions


if __name__ == "__main__":
    coordinates, connect = mol_info("../test/ethene")
    atoms, nlines = input_geom("../test/ethene")
    print atoms, '\n'
    print coordinates, '\n'
    print connect
    print translate(connect, atoms)
    #print pdist(coordinates)