import numpy as np
from scipy.interpolate import *
import read_parameters_ver2 as rp
import matplotlib.pyplot as plt

def exponential_spline(a, r):
    """The exponential spline for distances less than 1.2"""

    return np.exp(-1.0 * a[0] * r + a[1]) + a[2]

def polynomial_spline(r0, c, r, order):
    """The polynomial spline for distances greater than 1.2"""

    d = r - r0

    p = 0

    order = int(order)

    for i in range(0, order+1):
        p += c[i] * d ** i

    return p

def make_spline(npoints, r0, dr):
    """Creates the uninterpolated spline"""
    dict = rp.readfile("H-H")
    coeffs = dict["spline"]

    # construct exponential part

    spline = np.zeros(npoints)
    r = np.zeros(npoints)

    r[0] = r0
    for i in range(1, npoints):
        r[i] = r[i-1] + dr

    nints = len(coeffs)
    ints = np.zeros(nints)

    k = 0

    for key in coeffs:
        ints[k] = key
        k += 1

    ints.sort()
    r_cut = ints[-1]

    i = 0

    while i < npoints and r[i] < ints[0]:
        spline[i] = exponential_spline(coeffs[1.2], r[i])
        i += 1

    # construct polynomial part

    for j in range(1, nints-1):
        r0 = ints[j-1]
        while i < npoints and r[i] < ints[j]:
            spline[i] = polynomial_spline(r0, coeffs[ints[j]], 3, r[i])
            i += 1


    r0 = ints[-2]
    while i < npoints and r[i] < r_cut:
        spline[i] = polynomial_spline(r0, coeffs[ints[-1]], 5, r[i])
        i += 1
    return [r, spline]

def interpolate_spline(spline, r):
    """Interpolates a spline given from make_spline.

    Add the desired distances to interpolate over."""


    spline_new = interp1d(r, spline)
    spline_new2 = interp1d(r, spline, kind='cubic')

    #plt.plot(r, spline, 'o', r_new, spline_new(r_new), '-', r_new, spline_new2(r_new), '--')
    #plt.legend(['original', 'linear', 'cubic'])
    #plt.show()

    return spline_new2