import numpy as np
import linecache

class particle(object):
    def __init__(self, name, mass, occupancy, E ):
        self.name = name
        self.mass = mass
        self.occupancy = occupancy
        self.E = E
    def __repr__(self):
        return self.name
    def __str__(self):
        return "<%s>" % self.name



def readfile(interaction_pair):
    '''function to read data from a single parameter file
    ouputs into a dictionary structure, which will have
    an entry for each interaction pair. This function works for all atom
     pairs. Keys are:
        -'spline' contains spline coeff as documented in 'read_spline',
        -'Hcoeff' contains hamiltonian coefficients a 2D array, where a single column
      is in the form [r, H(r)]; H(r) is the hamiltonian coeff in the same
       order as specified in the input file documentation (see documentation folder).
        -"Scoeff" is in the same format as "Hcoeff".
        -"atom_info" key contains a list of objects which contain the
        information about an atom for a given interaction pair.'''

    atoms = interaction_pair.split('-') ##list of atom names

    dictionaray = {}
    filename = "../mio-1-1/" + interaction_pair + ".skf"


    with open(filename, "r") as file:
        lines = file.readlines()


    line1 = lines[0].split(',')


    grid_space = float(line1[0])
    grid_no = int(line1[1])

    matrix_start = get_matrix_start(lines)


    if atoms[0] == atoms[1]:

        dictionaray["atom_info"] = [get_atom_info(atoms[0])]
    else:
        atom_list=[]
        for atom in atoms:
            atom_list.append(get_atom_info(atom))
        dictionaray["atom_info"] = atom_list

    Hcoeff=[]
    Scoeff=[]

    ##read in parameters for H(r) and S(r), where r is distance##
    column_num = len(lines[matrix_start].split())
    parse1 = 0
    parse2 = int(column_num / 2.)
    for line in lines[matrix_start:matrix_start+grid_no]:
        words = line.split()
        Hcoeff.append(words[parse1+1:parse2])
        Scoeff.append(words[parse2+1:])


    Scoeff = make_matrix(Scoeff, grid_space, grid_no )
    Hcoeff = make_matrix(Hcoeff, grid_space, grid_no )

    ##load in spline parameters##

    dictionaray["spline"] = {}



    dictionaray["spline"] = read_spline(dictionaray["spline"], lines)

    ##store things in dictionary##
    dictionaray["Hcoeff"] = Hcoeff
    dictionaray["Scoeff"] = Scoeff
    return dictionaray


def read_spline(dictionary, lines):
    '''Returns a dictionary with all spline coefficeients contained within.
    Each key name is the upper distance interval of the coefficients (type=float), with
    the spline coeficients
    contained within the key.'''
    spline_start=0
    for num, line in enumerate(lines):
        if "Spline" in line:
            spline_start = num
            exit

    line2 = lines[spline_start + 1].split()
    n_int = int(line2[0]) #number for intpol sections
    cutoff = float(line2[1])  # cutoff of the repulsive interaction
    spline_dat = lines[spline_start+1:spline_start + 3+n_int]

    for ind, item in enumerate(spline_dat):
        spline_dat[ind] = item.split()
    distances = []
    splines = np.array(spline_dat[2:2+n_int - 1], dtype=float)
    splines = splines[:, 2:]

    exp_spline = np.array(spline_dat[1], dtype=float)

    spline_last = np.array(spline_dat[-1][2:], dtype=float)

    for line in spline_dat[2:2+n_int - 1]:
        distances.append(line[1])


    for ind, index in enumerate(distances):
        dictionary[float(distances[ind])] = splines[ind,:]

    dictionary[float(spline_dat[2][0])] = exp_spline
    dictionary[float(spline_dat[-1][1])] = spline_last

    return dictionary



##function to make a 2D array where a single row reads [r, H(r)/s(r)]##
def make_matrix(coeff, grid_space, grid_no):
    '''function to make a 2D array where a single row reads [r, H(r)/s(r)]
    where r is the internuclear distance'''
    r = np.arange(1, grid_no+1)*grid_space + 1.02
    coeff = np.array(coeff, dtype=float)
    matrix = np.concatenate((r[:, np.newaxis], coeff), axis=1)
    return matrix

def get_matrix_start(lines):
    """function detemines where the entries for the
    H matrix parameters and the S matrix parameters start"""
    matrix_start = 0
    for ind, line in enumerate(lines):
        words=line.split()
        first_delimiter = words[0]
        if ( "*" in first_delimiter and ( len(words) > 3 )):
            matrix_start = ind
            return matrix_start



def get_matrix_parameters(dictionary, key, dist):
    '''Function to get the H/S parameters for a
     given internuclear distance. This simply returns
     the H/S coefficients for the nearest distance nearest
     to dist. 'key' contians the key of the H/S parameters
     that you want to fetch'''
    matrix_param = 0
    coeff_list = dictionary[key]
    dr = coeff_list[1,0] - coeff_list[0,0]
    index = int(np.rint((dist-1.04)/dr))
    if (index < -1):
        return False
    if ( index < 0):
        index = 0
    matrix_param = coeff_list[index, 1:]
    return matrix_param

def get_spline_parameters(dictionary, dist):
    '''obselete'''
    splines = dictionary["spline"]
    if ( dist > splines["cutoff"]):
        return False

    coeff_list_qaud = splines["quad"]
    dr = coeff_list_qaud[0, 1] - coeff_list_qaud[0, 0]
    index = int(np.floor((dist - coeff_list_qaud[0, 0]) / dr))
    if ( index < 0):
        spline_param = splines['exp'][2:]
    elif (dist > coeff_list_qaud[-1, 1]):
        spline_param = splines['sext'][2:]
    else:
        spline_param = coeff_list_qaud[index, :]

    return spline_param

def get_atom_info(atom_name):
    '''function that takes an atom name e.g 'H' and finds the
    data about that atom in 'H-H.skf' (orbital energies, atom name and
     orbital occupancies, atom mass)  and feeds it onto an object.
     The orbital energies and occupancies are each in a 1D array
     in the format [d-orbital, p-orbital, s-orbital]'''
    filename = "../mio-1-1/" + atom_name + "-" + atom_name + ".skf"
    line2 = linecache.getline(filename, 2)
    line = linecache.getline(filename, 3)

    line2 = line2.split()

    for index, item in enumerate(line2):
        line2[index] = item.replace(",", "")

    mass = float(line.split()[0].replace(",", ""))

    E = np.array(line2[:3], dtype=float)
    occ = np.array(line2[-3:], dtype=float)
    atom = particle(atom_name, mass, occ, E)
    linecache.clearcache()
    return atom



if __name__ == "__main__":
    #example of code use, reads in data about C-H pair##
    dict1=  readfile("C-H")

